# TomAtom SmsGateway

### Usage

* set `SERVER_URI` const,

* call `encrypt` method:

> `SmsGatewayClientUtil::encrypt(clientId, privKey, data);`

* alternatively call `sendRaw` method (mainly for testing purpose):

> `SmsGatewayClientUtil::sendRaw(clientId, data);`

where _data_ is an associative array like this one:

> ```

> [

>    'sendTo' => '+420123456789',

>    'country' => 'cs',

>    'msg' => 'foo bar baz'

>  ];

> ```

(__sendTo__ can be with or without country prefix)

* you should get response like this in case of success:

> {

>  "status": "success",

>  "details": {

>       "id": 456,

>       "deviceId": null,

>       "messageUuid": "e1ff8-41cde-b3c60",

>       "sentTimestamp": 1234567890,

>       "from": null,

>       "sendTo": "775076596",

>       "message": "foo bar baz",

>       "sentResultCode": null,

>       "sentResultMessage": null,

>       "deliveredResultCode": null,

>       "deliveredResultMessage": null,

>       "type": "sent",

>       "sendNow": true,

>       "waitingForDeliveryInfo": null

>   }

> }

* or something like this when error occurs:

> {

>   "status": "error",

>   "details": "country denied for client"

> }

* in that case, check if you are sending valid `country` and that country corresponds with phone number in `sendTo`
