<?php

require_once 'SmsGatewayClientUtil.php';

use TomAtom\SmsGatewayBundle\Utils\SmsGatewayClientUtil;


// send sms example

$clientId = 'clientId';
$data = [
    'sendTo' => 'phoneNumber',
    'country' => 'cs',
    'msg' => 'message body',
];
//$result = SmsGatewayClientUtil::encrypt($clientId, $privKey, $data);
$result = SmsGatewayClientUtil::sendRaw($clientId, $data);

var_dump($result);
exit();
