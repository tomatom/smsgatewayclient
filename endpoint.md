# Endpoint description

### /api/send-raw

standard endpoint for recieving messages to send as sms

takes 4 arguments:

* `clientId` client Id,
* `sendTo` phone number with or without prefix,
* `country` country symbol (`cs`),
* `msg` message body

sent as POST form-data, with form named `data`. Request body may looks like this:
```
data[clientId]=clientId12345&data[sendTo]=777123456&data[country]=cs&data[msg]=message
```

on success, you should get json response like this:
```
{
 "status": "success",
 "details": {
   "id": 456,
   "deviceId": null,
   "messageUuid": "e1ff8-41cde-b3c60",
   "sentTimestamp": 1234567890,
   "from": null,
   "sendTo": "777123456",
   "message": "message",
   "sentResultCode": null,
   "sentResultMessage": null,
   "deliveredResultCode": null,
   "deliveredResultMessage": null,
   "type": "sent",
   "sendNow": true,
   "waitingForDeliveryInfo": null
  }
}
```
or something like this when error occurs:
```
{
  "status": "error",
  "details": "country denied for client"
}
```
in that case, check if you are sending valid `country` and that country corresponds with phone number in `sendTo`

### /api/send

endpoint for recieving encrypted messages

takes 3 arguments:

* `client` base64 encoded client Id,
* `data` base64 encoded encrypted data,
* `key` base64 encoded encrypted key,

sent as POST form-data, with form named `data`. Request body may looks like this:
```
data[client]=dGVzdDEyMw==&data[data]=A0e0KUynWmK64Gcm...Yeo&data[key]=gtXryEj35OowZoLU4s...hy0A
```

now, let's encrypt the `data` field:

* `data` for example:
```
[
    'sendTo' => '123456789',
    'country' => 'cs',
    'msg' => 'message body',
]
```

* stringify `data`

* generate new _encryption key_ (cryptographically secure random number) - for example `openssl_random_pseudo_bytes(32);`

* encrypt it with given _privKey_ - for example `openssl_private_encrypt($plainEncryptionKey, $encryptedEncryptionKey, $privKey);`

* get _initiation vector_ - for example `openssl_random_pseudo_bytes(openssl_cipher_iv_length(self::AES_256_CBC));`

* encrypt `data` with __plain__ _encryption key_ - for example `openssl_encrypt($data, self::AES_256_CBC, $plainEncryptionKey, 0, $iniciationVector);`

* append _iniciation vector_ to _encrypted_ `data`, with colon symbol `:` as glue - for example `$encryptedData = $encryptedData . ':' . $iniciationVector;`

* finally base64 encode __encrypted__ `data`, same for __encrypted__ _encryption key_ and clientId

responses from API are same like from `send-raw` endpoint
