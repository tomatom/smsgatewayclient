<?php

namespace TomAtom\SmsGatewayBundle\Utils;


class SmsGatewayClientUtil
{
    // set server uri
    const SERVER_URI = 'https://sms.tomatom.cz/api/send/';
    const SERVER_URI_RAW = 'https://sms.tomatom.cz/api/send-raw/';

    const AES_256_CBC = 'aes-256-cbc';

    /**
     * @param $clientId
     * @param $data
     *
     * @description data as []:
     * $data = [
     *  'sendTo' => '+420xxxxxxxxx',
     *  'country' => 'cs',
     *  'msg' => 'foo bar baz'
     *  ];
     *
     * @return array
     */
    public static function sendRaw($clientId, $data) {
        $data['clientId'] = $clientId;
//        var_dump($data);
        return self::send($data, self::SERVER_URI_RAW);
    }

    /**
     * @param $clientId
     * @param $privKey
     * @param $data
     *
     * @description data as []:
     * $data = [
     *  'sendTo' => '+420xxxxxxxxx',
     *  'country' => 'cs',
     *  'msg' => 'foo bar baz'
     *  ];
     *
     * @return array
     */
    public static function encrypt($clientId, $privKey, $data) {
        try {
            $plainEncryptionKey = openssl_random_pseudo_bytes(32);
            $encryptedEncryptionKey = '';
            openssl_private_encrypt($plainEncryptionKey, $encryptedEncryptionKey, $privKey);

            // get initiation vector
            $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length(self::AES_256_CBC));

            // make sure we get a string of data
            if(is_array($data)) {
                $data = json_encode($data);
            }

            // encrypt data with plain encryptionKey
            $encryptedData = openssl_encrypt($data, self::AES_256_CBC, $plainEncryptionKey, 0, $iv);

            // append iv to encData
            $encryptedData = $encryptedData . ':' . $iv;

            $dataToSend = [
                'client' => base64_encode($clientId),
                'data' => base64_encode($encryptedData),
                'key' => base64_encode($encryptedEncryptionKey)
            ];
//            var_dump($dataToSend);
            return self::send($dataToSend, self::SERVER_URI);
        } catch (\Exception $e) {
            return null;
        }
    }

    private static function send($data, $url) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
            'data' => $data
        ]));

        // receive server response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $serverResponse = curl_exec ($ch);

        curl_close ($ch);
        $err     = curl_errno($ch);
        $errmsg  = curl_error($ch);
//        var_dump($err);
//        var_dump($errmsg);
        // exit(var_dump($serverResponse));
        return $serverResponse;
    }
}
